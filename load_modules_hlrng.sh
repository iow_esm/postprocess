#!/bin/bash

module load cdo/2.2.2 # load cdo module before miniconda3, since cdo overwrites python path!!!!!
module load nco/5.1.6

module load miniconda3/22.11.1

# TODO to be removed when all necessary packages are installed globally
if [ `grep "# >>> conda initialize >>>" ~/.bashrc | wc -l` -eq 0 ]; then
    conda init bash
fi
source ~/.bashrc
conda activate iow_esm 



